package dev.agred.snake.util

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.utils.Logger
import com.badlogic.gdx.utils.viewport.Viewport

object ViewportUtils {

    private val log = Logger(ViewportUtils::class.java.name, Logger.DEBUG)

    private const val DEFAULT_CELL_SIZE = 1

    /**
     * Draws world grid for specified viewport using [ShapeRenderer].
     *
     * @param viewport The viewport. Required.
     * @param renderer ShapeRenderer that will be used for drawing. Required.
     * @param cellSize the size of cell in grid. If less than 1 by default 1 will be used.
     * @throws IllegalArgumentException if any param is null.
     */
    fun drawGrid(viewport: Viewport, renderer: ShapeRenderer, cellSize: Int = DEFAULT_CELL_SIZE) {
        var tmpCellSize = cellSize
        if (cellSize < DEFAULT_CELL_SIZE) {
            tmpCellSize = DEFAULT_CELL_SIZE
        }

        // copy old color from renderer
        val oldColor = Color(renderer.color)

        val worldWidth = viewport.worldWidth.toInt()
        val worldHeight = viewport.worldHeight.toInt()
        val doubleWorldWidth = worldWidth * 2
        val doubleWorldHeight = worldHeight * 2

        renderer.projectionMatrix = viewport.camera.combined
        renderer.begin(ShapeRenderer.ShapeType.Line)

        renderer.color = Color.WHITE

        // draw vertical lines
        var x = -doubleWorldWidth
        while (x < doubleWorldWidth) {
            renderer.line(x.toFloat(), (-doubleWorldHeight).toFloat(), x.toFloat(), doubleWorldHeight.toFloat())
            x += tmpCellSize
        }

        // draw horizontal lines
        var y = -doubleWorldHeight
        while (y < doubleWorldHeight) {
            renderer.line((-doubleWorldWidth).toFloat(), y.toFloat(), doubleWorldWidth.toFloat(), y.toFloat())
            y += tmpCellSize
        }

        // draw 0/0 lines
        renderer.color = Color.RED
        renderer.line(0f, (-doubleWorldHeight).toFloat(), 0f, doubleWorldHeight.toFloat())
        renderer.line((-doubleWorldWidth).toFloat(), 0f, doubleWorldWidth.toFloat(), 0f)

        // draw world bounds
        renderer.color = Color.GREEN
        renderer.line(0f, worldHeight.toFloat(), worldWidth.toFloat(), worldHeight.toFloat())
        renderer.line(worldWidth.toFloat(), 0f, worldWidth.toFloat(), worldHeight.toFloat())

        renderer.end()
        renderer.color = oldColor
    }

    /**
     * Prints pixels per unit for specified Viewport.
     *
     * @param viewport The viewport for which we want to print pixels per unit ratio. Required.
     * @throws IllegalArgumentException if viewport is null.
     */
    fun debugPixelsPerUnit(viewport: Viewport) {
        val screenWidth = viewport.screenWidth.toFloat()
        val screenHeight = viewport.screenHeight.toFloat()

        val worldWidth = viewport.worldWidth
        val worldHeight = viewport.worldHeight

        val xPPU = screenWidth / worldWidth
        val yPPU = screenHeight / worldHeight

        log.debug("x PPU= $xPPU yPPU= $yPPU")
    }
}