package dev.agred.snake.util

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Rectangle

fun ShapeRenderer.rect(rect: Rectangle) {
    this.rect(rect.x, rect.y, rect.width, rect.height)
}

fun ShapeRenderer.rect(rect: Rectangle, color: Color) {
    this.color = color
    this.rect(rect)
}