package dev.agred.snake.util

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import dev.agred.snake.entity.Direction

object GdxUtils {

    /**
     * Clears screen using specified [Color].
     *
     * @param color The color for clearing the screen. If null black will be used.
     */
    fun clearScreen(color: Color = Color.BLACK) {
        Gdx.gl.glClearColor(color.r, color.g, color.b, color.a)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
    }

    fun getInput(): Direction {
        val up = Gdx.input.isKeyPressed(Input.Keys.UP)
        val down = Gdx.input.isKeyPressed(Input.Keys.DOWN)
        val left = Gdx.input.isKeyPressed(Input.Keys.LEFT)
        val right = Gdx.input.isKeyPressed(Input.Keys.RIGHT)

        if (up)
            return Direction.UP
        if (down)
            return Direction.DOWN
        if (left)
            return Direction.LEFT
        if (right)
            return Direction.RIGHT
        return Direction.NONE
    }

}