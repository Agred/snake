package dev.agred.snake.util.debug

import com.badlogic.gdx.Application
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.Logger

class DebugCameraController {

    private val position = Vector2()
    private val startPosition = Vector2()
    private var zoom = 1.0f
    private var info: DebugCameraInfo = DebugCameraInfo()

    init {
        init()
    }

    // == init ==
    private fun init() {
        log.info("cameraInfo= $info")
    }

    // == public methods ==

    /**
     * Sets start position of camera.
     *
     * @param x the x position.
     * @param y the y position.
     */
    fun setStartPosition(x: Float, y: Float) {
        startPosition.set(x, y)
        setPosition(x, y)
    }

    /**
     * Applies internal position and zoom to specified camera.
     * Call this method after handling debug input.
     *
     * @param camera The camera instance.
     * @throws IllegalArgumentException if camera param is null.
     * @see .handleDebugInput
     */
    fun applyTo(camera: OrthographicCamera) {
        camera.position.set(position, 0f)
        camera.zoom = zoom
        camera.update()
    }

    /**
     * Handles debug input. Call this in you update cycle.
     *
     * @param delta the delta time.
     */
    fun handleDebugInput(delta: Float) {
        if (Gdx.app.type != Application.ApplicationType.Desktop) {
            return
        }

        val moveSpeed = info.moveSpeed * delta
        val zoomSpeed = info.zoomSpeed * delta

        // move control
        if (info.isLeftPressed) {
            moveLeft(moveSpeed)
        }

        if (info.isRightPressed) {
            moveRight(moveSpeed)
        }

        if (info.isUpPressed) {
            moveUp(moveSpeed)
        }

        if (info.isDownPressed) {
            moveDown(moveSpeed)
        }

        // zoom control
        if (info.isZoomInPressed) {
            zoomIn(zoomSpeed)
        }
        if (info.isZoomOutPressed) {
            zoomOut(zoomSpeed)
        }

        // reset control
        if (info.isResetPressed) {
            reset()
        }

        // log control
        if (info.isLogPressed) {
            logDebug()
        }

    }

    // == private methods ==
    private fun setPosition(x: Float, y: Float) {
        position.set(x, y)
    }

    private fun setZoom(value: Float) {
        zoom = MathUtils.clamp(value, info.maxZoomIn, info.maxZoomOut)
    }

    private fun moveCamera(xSpeed: Float, ySpeed: Float) {
        setPosition(position.x + xSpeed, position.y + ySpeed)
    }

    private fun moveLeft(speed: Float) {
        moveCamera(-speed, 0f)
    }

    private fun moveRight(speed: Float) {
        moveCamera(speed, 0f)
    }

    private fun moveUp(speed: Float) {
        moveCamera(0f, speed)
    }

    private fun moveDown(speed: Float) {
        moveCamera(0f, -speed)
    }

    private fun zoomIn(zoomSpeed: Float) {
        setZoom(zoom + zoomSpeed)
    }

    private fun zoomOut(zoomSpeed: Float) {
        setZoom(zoom - zoomSpeed)
    }

    private fun reset() {
        position.set(startPosition)
        setZoom(1.0f)
    }

    private fun logDebug() {
        log.debug("position= $position zoom= $zoom")
    }

    companion object {

        // == constants ==
        private val log = Logger(DebugCameraController::class.java.name, Logger.DEBUG)
    }
}
