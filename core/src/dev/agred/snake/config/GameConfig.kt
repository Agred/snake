package dev.agred.snake.config

class GameConfig {
    companion object {
        private const val Y_OFFSET = 2

        const val WIDTH: Float = 800f
        const val HEIGHT: Float = 480f
        const val WORLD_WIDTH: Float = 25f
        const val WORLD_HEIGHT: Float = 15f

        const val WORLD_CENTER_X: Float = WORLD_WIDTH / 2f
        const val WORLD_CENTER_Y: Float = WORLD_HEIGHT / 2f

        const val SNAKE_SIZE: Float = 1f

        const val MOVE_TIME: Float = 0.2f
        const val SNAKE_SPEED: Float = 1f

        const val COIN_SIZE: Float = 1f
        const val COIN_SCORE: Int = 20

        const val HUD_WIDTH: Float = 800f
        const val HUD_HEIGHT: Float = 480f

        const val MAX_Y = WORLD_HEIGHT - Y_OFFSET
    }

}