package dev.agred.snake.config

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Preferences
import dev.agred.snake.SnakeGame
import dev.agred.snake.common.GameState

object GameManager {
    const val HIGH_SCORE_KEY: String = "highscore"

    private var state: GameState = GameState.READY

    var score: Int = 0
        set(value) {
            field = value
            if (field >= highScore)
                highScore = field
        }
    var displayScore: Int = 0
    var highScore: Int = 0
    var displayHighScore: Int = 0

    private val preferences: Preferences = Gdx.app.getPreferences(SnakeGame::class.java.simpleName)

    init {
        highScore = preferences.getInteger(HIGH_SCORE_KEY, 0)
    }

    fun isReady(): Boolean {
        return state.isReady()
    }

    fun isPlaying(): Boolean {
        return state.isPlaying()
    }

    fun isGameOver(): Boolean {
        return state.isGameOver()
    }

    fun finishGame() {
        saveHighScore()
        state = GameState.GAME_OVER
    }

    fun reset() {
        startGame()
        score = 0
        displayScore = 0
    }

    fun updateDisplayScore(delta: Float) {
        if (displayScore < score)
            displayScore = Math.min(score, displayScore + (100 * delta).toInt())

        if (displayHighScore < highScore)
            displayHighScore = Math.min(highScore, displayHighScore + (100 * delta).toInt())
    }

    private fun startGame() {
        state = GameState.PLAYING
    }

    private fun saveHighScore() {
        if (score < highScore)
            return

        highScore = score
        preferences.putInteger(HIGH_SCORE_KEY, highScore)
        preferences.flush()
    }
}