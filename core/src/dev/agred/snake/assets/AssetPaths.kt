package dev.agred.snake.assets

object AssetPaths {
    const val UI_FONT: String = "fonts/oswald-32.fnt"
    const val UI_SKIN: String = "ui/ui.json"
    const val GAMEPLAY: String = "gameplay/gameplay.atlas"
}