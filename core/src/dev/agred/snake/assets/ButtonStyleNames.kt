package dev.agred.snake.assets

object ButtonStyleNames {
    const val PLAY: String = "play"
    const val QUIT: String = "quit"
}