package dev.agred.snake.assets

object RegionNames {
    const val BACKGROUND: String = "background"
    const val BODY: String = "body"
    const val COIN: String = "coin"
    const val HEAD: String = "head"
}