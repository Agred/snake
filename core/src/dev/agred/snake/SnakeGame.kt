package dev.agred.snake

import com.badlogic.gdx.Application
import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.utils.Logger
import dev.agred.snake.screen.loading.LoadingScreen

class SnakeGame : Game() {

    lateinit var assetManager: AssetManager
    lateinit var batch: SpriteBatch

    override fun create() {
        assetManager = AssetManager()
        batch = SpriteBatch()

        Gdx.app.logLevel = Application.LOG_DEBUG
        assetManager.logger.level = Logger.DEBUG

        setScreen(LoadingScreen(this))
    }

    override fun dispose() {
        super.dispose()
        assetManager.dispose()
        batch.dispose()
    }
}
