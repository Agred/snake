package dev.agred.snake.entity

import dev.agred.snake.config.GameConfig

class BodyPart : EntityBase() {

    var justAdded: Boolean = true

    init {
        setSize(GameConfig.SNAKE_SIZE)
    }
}