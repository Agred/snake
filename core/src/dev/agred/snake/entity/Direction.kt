package dev.agred.snake.entity

enum class Direction {
    NONE,
    UP,
    DOWN,
    LEFT,
    RIGHT;

    fun isNone(): Boolean {
        return this == NONE
    }

    fun isUp(): Boolean {
        return this == UP
    }

    fun isDown(): Boolean {
        return this == DOWN
    }

    fun isLeft(): Boolean {
        return this == LEFT
    }

    fun isRight(): Boolean {
        return this == RIGHT
    }

    fun getOpposite(): Direction {
        if (isLeft())
            return RIGHT
        if (isRight())
            return LEFT
        if (isUp())
            return DOWN
        if (isDown())
            return UP
        if (isNone())
            return NONE
        throw IllegalArgumentException()
    }

    fun isOpposite(direction: Direction): Boolean {
        return direction == getOpposite()
    }
}