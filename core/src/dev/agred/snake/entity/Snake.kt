package dev.agred.snake.entity

import com.badlogic.gdx.math.Rectangle
import dev.agred.snake.config.GameConfig

class Snake {

    val x: Float
        get() = head.x
    val y: Float
        get() = head.y

    val bounds: Rectangle
        get() = head.bounds

    val body: List<BodyPart>
        get() = bodyParts

    private val head: SnakeHead = SnakeHead()

    private var direction: Direction = Direction.RIGHT
    private var bodyParts: MutableList<BodyPart> = mutableListOf()

    private var xBeforeUpdate: Float = 0f
    private var yBeforeUpdate: Float = 0f

    fun move(direction: Direction) {
        xBeforeUpdate = x
        yBeforeUpdate = y

        if (!direction.isNone() && !isBackflipping(direction))
            this.direction = direction

        if (this.direction.isUp())
            head.updateY(GameConfig.SNAKE_SPEED)
        else if (this.direction.isDown())
            head.updateY(-GameConfig.SNAKE_SPEED)
        else if (this.direction.isLeft())
            head.updateX(-GameConfig.SNAKE_SPEED)
        else if (this.direction.isRight())
            head.updateX(GameConfig.SNAKE_SPEED)

        updateBodyParts()
    }

    fun outOfBoundsX() {
        head.x = if (head.x >= GameConfig.WORLD_WIDTH)
            0f
        else
            GameConfig.WORLD_WIDTH - GameConfig.SNAKE_SPEED
    }

    fun outOfBoundsY() {
        head.y = if (head.y >= GameConfig.MAX_Y)
            0f
        else
            GameConfig.MAX_Y - GameConfig.SNAKE_SPEED
    }

    fun addBodyPart() {
        val bodyPart = BodyPart()
        bodyPart.setPosition(x, y)
        bodyParts.add(0, bodyPart)
    }

    fun reset() {
        bodyParts.clear()
        xBeforeUpdate = 0f
        yBeforeUpdate = 0f
        head.setPosition(0f, 0f)
    }

    private fun updateBodyParts() {
        if (bodyParts.size > 0) {
            val tail = bodyParts.removeAt(0)
            tail.setPosition(xBeforeUpdate, yBeforeUpdate)
            bodyParts.add(tail)
        }
    }

    private fun isBackflipping(direction: Direction): Boolean {
        return this.direction.isOpposite(direction) && bodyParts.isNotEmpty()
    }

}