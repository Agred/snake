package dev.agred.snake.entity

import com.badlogic.gdx.math.MathUtils
import dev.agred.snake.config.GameConfig

class Coin : EntityBase() {

    var isAvailable: Boolean = false

    init {
        setSize(GameConfig.COIN_SIZE)
    }

    fun spawn() {
        val coinX = MathUtils.random(GameConfig.WORLD_WIDTH.toInt() - GameConfig.COIN_SIZE)
        val coinY = MathUtils.random(GameConfig.MAX_Y.toInt() - GameConfig.COIN_SIZE)
        isAvailable = true
        setPosition(coinX, coinY)
    }
}