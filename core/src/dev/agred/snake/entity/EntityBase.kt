package dev.agred.snake.entity

import com.badlogic.gdx.math.Rectangle

abstract class EntityBase {

    var x: Float = 0f
    var y: Float = 0f
    var width: Float = 1f
    var height: Float = 1f
    var bounds: Rectangle = Rectangle(x, y, width, height)

    init {
        updateBounds()
    }

    fun setPosition(x: Float, y: Float) {
        this.x = x
        this.y = y
        updateBounds()
    }

    protected fun setSize(size: Float) {
        this.width = size
        this.height = size
        updateBounds()
    }

    protected fun setSize(width: Float, height: Float) {
        this.width = width
        this.height = height
        updateBounds()
    }

    protected fun updateBounds() {
        bounds.setPosition(x, y)
        bounds.setSize(width, height)
    }
}