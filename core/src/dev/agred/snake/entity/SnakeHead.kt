package dev.agred.snake.entity

import dev.agred.snake.config.GameConfig

class SnakeHead : EntityBase() {

    init {
        setSize(GameConfig.SNAKE_SIZE)
    }

    fun updateX(amount: Float) {
        x += amount
        updateBounds()
    }

    fun updateY(amount: Float) {
        y += amount
        updateBounds()
    }
}