package dev.agred.snake.common

enum class GameState {
    READY,
    PLAYING,
    GAME_OVER;

    fun isReady(): Boolean {
        return this == READY
    }

    fun isPlaying(): Boolean {
        return this == PLAYING
    }

    fun isGameOver(): Boolean {
        return this == GAME_OVER
    }
}