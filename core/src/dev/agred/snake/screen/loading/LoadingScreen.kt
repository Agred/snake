package dev.agred.snake.screen.loading

import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.utils.viewport.FitViewport
import dev.agred.snake.SnakeGame
import dev.agred.snake.assets.AssetDescriptors
import dev.agred.snake.config.GameConfig
import dev.agred.snake.screen.game.GameScreen
import dev.agred.snake.util.GdxUtils

class LoadingScreen(private val game: SnakeGame) : ScreenAdapter() {
    companion object {
        const val PROGRESS_BAR_WIDTH = GameConfig.HUD_WIDTH / 2
        const val PROGRESS_BAR_HEIGHT = 60f
    }


    private val camera: OrthographicCamera = OrthographicCamera()
    private val viewport: FitViewport = FitViewport(GameConfig.HUD_WIDTH, GameConfig.HUD_HEIGHT, camera)
    private val renderer: ShapeRenderer = ShapeRenderer()

    private val manager: AssetManager = game.assetManager

    private var progress: Float = 0f
    private var waitTime: Float = 0.75f
    private var changeScreen: Boolean = false

    override fun show() {
        manager.load(AssetDescriptors.UI_FONT)
        manager.load(AssetDescriptors.GAMEPLAY)
        manager.load(AssetDescriptors.UI_SKIN)
    }

    override fun render(delta: Float) {
        GdxUtils.clearScreen()
        update(delta)
        draw()

        if (changeScreen)
            game.screen = GameScreen(game)
    }

    private fun draw() {
        viewport.apply()
        renderer.projectionMatrix = camera.combined
        renderer.begin(ShapeRenderer.ShapeType.Filled)
        renderer.rect(
                (GameConfig.HUD_WIDTH - PROGRESS_BAR_WIDTH) / 2f,
                (GameConfig.HUD_HEIGHT - PROGRESS_BAR_HEIGHT) / 2f,
                progress * PROGRESS_BAR_WIDTH,
                PROGRESS_BAR_HEIGHT
        )
        renderer.end()
    }

    private fun update(delta: Float) {
        progress = manager.progress
        if (!manager.update())
            return
        waitTime -= delta
        if (waitTime <= 0)
            changeScreen = true
    }

    override fun resize(width: Int, height: Int) {
        viewport.update(width, height, true)
    }

    override fun hide() {
        dispose()
    }

    override fun dispose() {
        renderer.dispose()
    }
}