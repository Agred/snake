package dev.agred.snake.screen.game

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.math.Intersector
import dev.agred.snake.config.GameConfig
import dev.agred.snake.config.GameManager
import dev.agred.snake.entity.Coin
import dev.agred.snake.entity.Snake
import dev.agred.snake.util.GdxUtils

class GameController(val snake: Snake = Snake(), val coin: Coin = Coin()) {

    private var timer: Float = 0f

    fun update(delta: Float) {
        if (!GameManager.isPlaying()) {
            checkForRestart()
            return
        }

        GameManager.updateDisplayScore(delta)

        checkOutOfBounds()
        val input = GdxUtils.getInput()

        timer += delta
        if (timer < GameConfig.MOVE_TIME)
            return

        checkCollision()

        timer = 0f
        snake.move(input)
        spawnCoin()
    }

    private fun spawnCoin() {
        if (coin.isAvailable)
            return
        coin.spawn()
    }

    private fun checkOutOfBounds() {
        if (snake.x >= GameConfig.WORLD_WIDTH || snake.x < 0)
            snake.outOfBoundsX()
        if (snake.y >= GameConfig.MAX_Y || snake.y < 0)
            snake.outOfBoundsY()
    }

    private fun checkCollision() {
        val overlapsWithCoin = Intersector.overlaps(snake.bounds, coin.bounds)
        if (coin.isAvailable && overlapsWithCoin) {
            snake.addBodyPart()
            coin.isAvailable = false
            GameManager.score += GameConfig.COIN_SCORE
        }
        checkCollisionWithBodyParts()
    }

    private fun checkCollisionWithBodyParts() {
        for (bodyPart in snake.body) {
            if (bodyPart.justAdded) {
                bodyPart.justAdded = false
                continue
            }

            if (Intersector.overlaps(snake.bounds, bodyPart.bounds))
                GameManager.finishGame()
        }
    }

    private fun checkForRestart() {
        if (Gdx.input.isKeyPressed(Input.Keys.SPACE) || Gdx.input.isTouched)
            restart()
    }

    private fun restart() {
        GameManager.reset()
        snake.reset()
        coin.isAvailable = false
        timer = 0f
    }
}