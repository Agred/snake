package dev.agred.snake.screen.game

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.GlyphLayout
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.utils.Disposable
import com.badlogic.gdx.utils.viewport.FitViewport
import dev.agred.snake.assets.AssetDescriptors
import dev.agred.snake.assets.RegionNames
import dev.agred.snake.config.GameConfig
import dev.agred.snake.config.GameManager
import dev.agred.snake.util.GdxUtils
import dev.agred.snake.util.ViewportUtils
import dev.agred.snake.util.debug.DebugCameraController
import dev.agred.snake.util.rect

class GameRenderer(
        private val batch: SpriteBatch,
        manager: AssetManager,
        private val controller: GameController) : Disposable {

    companion object {
        const val PADDING = 20f
    }

    private val camera: OrthographicCamera = OrthographicCamera()
    private val debugCam: DebugCameraController = DebugCameraController()

    private val viewport: FitViewport = FitViewport(GameConfig.WORLD_WIDTH, GameConfig.WORLD_HEIGHT, camera)
    private val hud: FitViewport = FitViewport(GameConfig.HUD_WIDTH, GameConfig.HUD_HEIGHT)

    private val renderer: ShapeRenderer = ShapeRenderer()

    private val font: BitmapFont = manager.get(AssetDescriptors.UI_FONT)
    private val layout: GlyphLayout = GlyphLayout()

    private val backgroundRegion: TextureRegion
    private val bodyRegion: TextureRegion
    private val coinRegion: TextureRegion
    private val headRegion: TextureRegion

    init {
        debugCam.setStartPosition(GameConfig.WORLD_CENTER_X, GameConfig.WORLD_CENTER_Y)

        val atlas = manager.get(AssetDescriptors.GAMEPLAY)
        backgroundRegion = atlas.findRegion(RegionNames.BACKGROUND)
        bodyRegion = atlas.findRegion(RegionNames.BODY)
        coinRegion = atlas.findRegion(RegionNames.COIN)
        headRegion = atlas.findRegion(RegionNames.HEAD)
    }


    fun render(delta: Float) {
        debugCam.handleDebugInput(delta)
        debugCam.applyTo(camera)

        GdxUtils.clearScreen()

        renderGameplay()
        renderHud()
        renderDebug()
    }


    fun resize(width: Int, height: Int) {
        viewport.update(width, height, true)
        hud.update(width, height, true)
        ViewportUtils.debugPixelsPerUnit(viewport)
        ViewportUtils.debugPixelsPerUnit(hud)
    }

    private fun renderGameplay() {
        viewport.apply()
        batch.projectionMatrix = camera.combined
        batch.begin()
        drawGameplay()
        batch.end()
    }

    private fun drawGameplay() {
        batch.draw(backgroundRegion, 0f, 0f, GameConfig.WORLD_WIDTH, GameConfig.WORLD_HEIGHT)
        val coin = controller.coin
        if (coin.isAvailable)
            batch.draw(coinRegion, coin.bounds.x, coin.bounds.y, GameConfig.COIN_SIZE, GameConfig.COIN_SIZE)

        val snake = controller.snake
        for (bodyPart in snake.body)
            batch.draw(bodyRegion, bodyPart.x, bodyPart.y, bodyPart.width, bodyPart.height)
        batch.draw(headRegion, snake.x, snake.y, snake.bounds.width, snake.bounds.height)
    }

    private fun renderDebug() {
        ViewportUtils.drawGrid(viewport, renderer)
        viewport.apply()

        val oldColor = Color(renderer.color)

        renderer.projectionMatrix = camera.combined

        renderer.begin(ShapeRenderer.ShapeType.Line)
        drawDebug()
        renderer.end()

        renderer.color = oldColor

    }

    private fun drawDebug() {
        renderer.color = Color.FOREST
        for (bodyPart in controller.snake.body)
            renderer.rect(bodyPart.bounds)

        renderer.rect(controller.snake.bounds, Color.GREEN)

        if (controller.coin.isAvailable)
            renderer.rect(controller.coin.bounds, Color.GOLD)
    }

    private fun renderHud() {
        hud.apply()
        batch.projectionMatrix = hud.camera.combined
        batch.begin()
        drawHud()
        batch.end()
    }

    private fun drawHud() {
        val highScore = "HIGH SCORE: ${GameManager.displayHighScore}"
        layout.setText(font, highScore)
        font.draw(batch, layout, PADDING, hud.worldHeight - PADDING)

        val scoreX = hud.worldWidth - layout.width
        val scoreY = hud.worldHeight - PADDING
        val score = "SCORE: ${GameManager.displayScore}"
        layout.setText(font, score)
        font.draw(batch, layout, scoreX, scoreY)
    }

    override fun dispose() {
        renderer.dispose()
    }
}