package dev.agred.snake.screen.game

import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import dev.agred.snake.SnakeGame

class GameScreen(game: SnakeGame) : ScreenAdapter() {

    private val manager: AssetManager = game.assetManager
    private val batch: SpriteBatch = game.batch
    private val controller: GameController = GameController()
    private val renderer: GameRenderer = GameRenderer(batch, manager, controller)

    override fun render(delta: Float) {
        controller.update(delta)
        renderer.render(delta)
    }

    override fun resize(width: Int, height: Int) {
        renderer.resize(width, height)
    }

    override fun hide() {
        dispose()
    }

    override fun dispose() {
        renderer.dispose()
    }
}