package dev.agred.snake.desktop

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import dev.agred.snake.SnakeGame
import dev.agred.snake.config.GameConfig

object DesktopLauncher {
    @JvmStatic
    fun main(arg: Array<String>) {
        val config = LwjglApplicationConfiguration()
        config.width = GameConfig.WIDTH.toInt()
        config.height = GameConfig.HEIGHT.toInt()
        LwjglApplication(SnakeGame(), config)
    }
}
