package dev.agred.snake.desktop

import com.badlogic.gdx.tools.texturepacker.TexturePacker

class AssetPacker {
    companion object {
        private const val RAW_ASSETS_PATH = "desktop/assets-raw"
        private const val ASSETS_PATH = "android/assets"
        private const val GAMEPLAY = "gameplay"
        private const val UI = "ui"

        @JvmStatic
        fun main(args: Array<String>) {
            TexturePacker.process(
                    "$RAW_ASSETS_PATH/$GAMEPLAY",
                    "$ASSETS_PATH/$GAMEPLAY",
                    GAMEPLAY
            )

            TexturePacker.process(
                    "$RAW_ASSETS_PATH/$UI",
                    "$ASSETS_PATH/$UI",
                    UI
            )
        }
    }
}